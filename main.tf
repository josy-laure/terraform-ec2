terraform {
  required_providers {
    aws = {
      source  = “hashicorp/aws”
      version = “~> 4.16"
    }
  }
}
provider “aws” {
  region  = “us-east-1"
}
resource “aws_instance” “myec2" {
  ami           = var.ami
  instance_type = var.instance_type
  tags = {
    Name = “python-app”
  }
}
terraform {
  backend “s3" {
    bucket = “project02-24344545”
    key    = “test-bucket”
    region = “us-east-1”
  }
  }
